#!/bin/sh

# This program may be freely distributed provided that all
# copyright notices are retained.

#
# creating a localised live cd 
# Marcus Bauer 2005
# Luis Villa 2005
#

# steps involved:
# ---------------
# 1. extract live cd iso-image and uncompress file system
# 2. alter contents: packages, branding & configs
# 3. create new iso-image


# pre-requisites:
# ---------------
# apt-get install cloop-utils rsync mkisofs
# wget http://us.releases.ubuntu.com/releases/5.04/ubuntu-5.04-live-i386.iso
#
# needed: (cf. README) 
# - isolinux.txt, f[1-10].txt 
# - boot splash ppmtolss16 < splash.ppm > splash.rle
# - gnome splash
# - gnomebackground, xscreensaver
# - locale.gen
# - additional country specific .deb: man-pages, howto, input(chinese) etc.
# - desktop docs (startpage browser, info stuff)
# - mount -t nfs 192.168.5.1:/tmp mnt_livecd_iso/ (if you don't have enough 
#   space on your local harddisk)


# =========================================================================
# configuration
# =========================================================================

# all configuration is done in "livecd.conf" - nothing should be changed here

. livecd.conf

if [ "$LANG_LIVE" != "" ]; then
	. locales/${LANG_LIVE}/packages.cfg
fi
# end of configuration ====================================================


export REMOVELIST
export ADDLIST

export LC_ALL=C
export LIVECD
export LANG_LIVE
export LIVECD_TITLE
export LOCALE_GEN



#
# usage stuff
#

DEBUG=$3

ME=`whoami`

if ! [ "$ME" = "root" ]; then
	echo
	echo " * you must be root, before starting the process * "
	echo
	exit
fi


function print_usage {
	echo 
	echo "  usage: make_livecd.sh en en_US [debug|nodebug [burn [reboot]]]"
	echo
	echo "  Substitute 'en_US' and 'en' with your desired locale. Check the"
	echo "  README for more information."
	echo
}

if [ -z "$1" ]; then 
	print_usage
	exit
fi

if [ -z "$2" ]; then
	print_usage
	exit
fi


# check if live CD iso is there
if ! test -f $LIVECD; then
	echo
	echo
	echo "  Could not find < $LIVECD >"
	echo
	echo "  You need to download it first and save it in iso/"
	echo
	echo "  Get it from here:"
	echo -n "  http://us.releases.ubuntu.com/"
	echo "releases/5.04/ubuntu-5.04-live-i386.iso"
	echo
	echo
	exit
fi



# TODO: remove me
echo "Packages to be removed: $REMOVELIST"
echo "Packages to be added: $ADDLIST"



#=====================================================================
#
# 1. extract live cd
#
#=====================================================================


mount -o loop $LIVECD mnt_livecd_iso/

echo "extracting liveCD..."
time \
rsync --exclude=/casper/filesystem.cloop -a mnt_livecd_iso/ extracted_cd

echo "uncompressing filesystem - this may take about 5 minutes"
time \
extract_compressed_fs mnt_livecd_iso/casper/filesystem.cloop > uncompressed_fs \
2> /dev/null

umount mnt_livecd_iso/



if [ "$DEBUG" = "debug" ]; then
        echo "step 1 finished - liveCD extracted. Press enter when ready to proceed"
        read
fi



#=====================================================================
#
# 2. alter contents of liveCD
#
#=====================================================================

mount -o loop uncompressed_fs mnt_livecd_fs/
mount -t proc proc mnt_livecd_fs/proc
mount -t sysfs sysfs mnt_livecd_fs/sys


#========================================================================
# change language & preseedings 
#========================================================================


# append local-preseeding to APPEND-line in isolinux.cfg

sed -i "s/rw --$/rw preseed\/locale=$LANG_LIVE --/"  	\
	extracted_cd/isolinux/isolinux.cfg

# change boot/installer texts
cp locales/${LANG_LIVE}/isolinux.txt extracted_cd/isolinux/
cp locales/${LANG_LIVE}/f?.txt extracted_cd/isolinux/

cp locales/${LANG_LIVE}/xscreensaver mnt_livecd_fs/etc/skel/.xscreensaver

# skel: flash, videos, homepage # TODO

# resolv.conf, sources.list for apt-get
cp /etc/resolv.conf mnt_livecd_fs/etc
cp sources.list mnt_livecd_fs/etc/apt/

# copy branding stuff and deb-packages into 
#TODO clean up
cp locales/${LANG_LIVE}/*.deb mnt_livecd_fs/tmp/
cp locales/${LANG_LIVE}/splash.rle extracted_cd/isolinux/
cp locales/${LANG_LIVE}/gnome-splash1.png mnt_livecd_fs/usr/share/pixmaps/splash/
cp locales/${LANG_LIVE}/gnome-background.jpg mnt_livecd_fs/usr/share/pixmaps/

# set up the homedir; put it here instead of /etc/skel to avoid
# copying time on startup
cp -r locales/${LANG_LIVE}/home mnt_livecd_fs/home/ubuntu 
# TODO: check if this is reliable
chown -R 1000:1000 mnt_livecd_fs/home/ubuntu/

echo $LOCALE_GEN > mnt_livecd_fs/etc/locale.gen

# TODO: cp debs; no link
# link cached *.deb's into the livecd_fs
cp cache/*.deb mnt_livecd_fs/var/cache/apt/archives/



if [ "$DEBUG" = "debug" ]; then
        echo "Step 2 finished - changes w/o need of chroot done. Press enter when ready to proceed"
        read
fi



# ==== CHROOT STARTS HERE ====================================================

cp do_changes_in_chroot.sh mnt_livecd_fs/tmp
chroot mnt_livecd_fs/ tmp/do_changes_in_chroot.sh $1

# ============== REMOVING PACKAGES ===============
# ============== INSTALLING PACKAGES =============
# ============== CONFIGURING GNOME ===============

# ==== END OF CHROOT =========================================================


if [ "$DEBUG" = "debug" ]; then
        echo "Changes in chroot done. Press enter when ready to proceed"
        read 
fi



# gdm background color & theme
sed -i "s/^BackgroundColor=.*$/BackgroundColor=#2A569D/" mnt_livecd_fs/etc/gdm/gdm.conf

# gdm theme 
sed -i "s/^GraphicalTheme=Human/GraphicalTheme=circles/" mnt_livecd_fs/etc/gdm/gdm.conf

# app defaults
sed -i "s,firefox.desktop,epiphany.desktop," mnt_livecd_fs/etc/gnome/defaults.list
sed -i "s,ooo645writer.desktop,abiword.desktop," mnt_livecd_fs/etc/gnome/defaults.list

# change default panel launcher from firefox to evince
sed -i "s,firefox.desktop,epiphany.desktop," mnt_livecd_fs/etc/gconf/gconf.xml.defaults/apps/panel/default_setup/objects/browser_launcher/%gconf.xml
sed -i "s,firefox.desktop,epiphany.desktop," mnt_livecd_fs/usr/share/gconf/schemas/panel-default-setup.entries
sed -i "s,firefox.desktop,epiphany.desktop," mnt_livecd_fs/usr/share/gconf/schemas/panel-default-setup-laptop.entries

# firefox startpage empty
cp locales/templates/index.html mnt_livecd_fs/usr/share/ubuntu-artwork/home/


chroot mnt_livecd_fs/ dpkg-query -W --showformat='${Package} ${Version}\n' > extracted_cd/casper/filesystem.manifest

if [ "$DEBUG" = "debug" ]; then
        echo "All changes done. Press enter when ready to proceed"
        read 
fi

# clean up #TODO
rm mnt_livecd_fs/root/.bash_history
rm mnt_livecd_fs/etc/resolv.conf
rm -rf mnt_livecd_fs/tmp/*

# this saves us downloading the *.debs again and again during development
mv mnt_livecd_fs/var/cache/apt/archives/*.deb cache/

umount mnt_livecd_fs/proc
umount mnt_livecd_fs/sys
umount mnt_livecd_fs/ # lsof mnt_livecd_fs; lsof -p 12345


# removing not needed stuff
rm -r extracted_cd/programs/thunderbird/
rm -r extracted_cd/programs/gimp/
rm -r extracted_cd/programs/openoffice/
rm -r extracted_cd/programs/firefox/
rm extracted_cd/autorun.inf 
rm extracted_cd/start.exe   

# adding windows programs
cp locales/winprog/* extracted_cd/programs/

#=====================================================================
#
# 3. create new iso image
#
#=====================================================================

#---------------------------------------------------------------------
# zero out the FS and save space (otherwise compression does not work)
#---------------------------------------------------------------------

# create temporary virgin fs
echo "creating temporary virgin filesystem"
mkdir mnt_livecd_fs.zeroed
time \
dd if=/dev/zero of=uncompressed_fs.zeroed bs=1M count=2000
mkfs.ext2 -F uncompressed_fs.zeroed

# rsync into virgin fs 
echo "rsync'ing all data into the virgin fs"
mount -o loop,noatime,nodiratime uncompressed_fs mnt_livecd_fs
mount -o loop,noatime,nodiratime uncompressed_fs.zeroed mnt_livecd_fs.zeroed
rsync -ax mnt_livecd_fs/. mnt_livecd_fs.zeroed/.

# umount and clean up
umount mnt_livecd_fs mnt_livecd_fs.zeroed
rmdir mnt_livecd_fs.zeroed




# note: add additional swap if ncessary - you'll need 700MB free mem...

#--best # this switch may be added: takes ages but saves arount 15MB
time \
create_compressed_fs  uncompressed_fs.zeroed 65536 >  \
	extracted_cd/casper/filesystem.cloop



echo "creating md5sums"
(cd extracted_cd && find . -type f -print0 | xargs -0 md5sum > md5sum.txt)


echo "creating iso image"
mkisofs -r -V "$LIVECD_TITLE" \
            -cache-inodes \
            -J -l -b isolinux/isolinux.bin \
            -c isolinux/boot.cat -no-emul-boot \
            -boot-load-size 4 -boot-info-table \
            -o $LIVECD_NAME extracted_cd/


echo "creating md5sum of iso image"
md5sum $LIVECD_NAME


if  [ "$4" = "burn" ]; then
	cdrecord dev=/dev/hdc blank=fast -v -data -nopad $LIVECD_NAME
fi



# clean up uncompressed_fs extracted_cd 
rm -r extracted_*
rm  uncompressed_fs

if  [ "$5" = "reboot" ]; then
	reboot
fi
