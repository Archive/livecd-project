HOWTO create a localized live CD
================================

Prerequisites
-------------

Install the following packages (here the debian names):
cloop-utils rsync mkisofs msttcorefonts syslinux

Make sure you have enough memory, you need 700MB free; a swapfile is
easily added:
  dd if=/dev/zero of=swapfile bs=1024 count=700k
  mkswap -c swapfile
  swapon swapfile

Check out: http://cvs.gnome.org/viewcvs/livecd-project/
This will create under livecd-project/ the necessary directories
and contains stuff for localization and branding.

Next you need to download the live CD iso from ubuntu:
http://us.releases.ubuntu.com/releases/5.04/ubuntu-5.04-live-i386.iso and save
it to the iso/ subdirectory
(if you are short of bandwidth and just want to help localization you
can skip this)

Change to livecd/locales and create a directory with the name of your
locale (i.e. pt_BR or no_NO) and copy everything from en_US into it.

Translations
------------
You need to translate all the text files, at least isolinux.txt and
f1.txt; don't worry that doesn't take more than 10 minutes. Note: only ASCII characters allowed.

Changing images
---------------
boot-splash:
* open gnome-boot-splash-it.xcf in gimp
  the text is in separate layers which makes changes easy
  font is trebuchet bold
* make your changes and save in .xcf to be able to reuse it
* merge visible layers (it is important to merge before indexing)
	Image->merge visible layers
* image->mode->indexed->16 colors, dithering: none
* save as .ppm, raw mode
* now run on the command line:
  ppmtolss16 < gnome-boot-splash-XY.ppm > splash.rle
  XY=your language, i.e. pt, no etc.

gnome-backgound:
* open gnome-background-XY.xcf in gimp
* make your changes an save as .jpg

To add additional software, add them the .deb packages to your
directory. manpages-XY and doc-linux-XY may be a good starting point.

You may download a copy of the latest firefox for windows and save it to
the winprogs directory. Thus after a demonstration of the gnome liveCD
you can install a first open source program on that computer you ran the
liveCD on (which most likly is running windows...)

Adding sample files to the Desktop
----------------------------------
You simply need to copy everything into the Desktop/ directory in your
locales sub directory, i.e. into locales/en_US/Desktop/



FINISHED. You are done!

====================================================
Now change back to livecd/ and run: ./make_livecd.sh
====================================================

Depending on the speed of your Computer and network connection it takes
about an hour to create the new .iso.
