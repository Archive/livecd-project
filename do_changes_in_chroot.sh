#!/bin/sh

# Version: 0.2.0

# 
# making all the changes to the cd while being chrooted to it
#
# manually: chroot mnt_livecd_fs/ /bin/sh


# ===================================================================
# Configuration
# ===================================================================


LANG_PACKAGES="language-pack-${1} language-support-${1}"
#TODO clean up echo "Language packages: $LANG_PACKAGES"

# ===================================================================
# end of configuration
# ===================================================================


# some preparation

export LANG_PACKAGES
export LC_ALL=C

apt-get update

#
# some preparation
#

# locale needs to be generated as we have copied a new /etc/locale.gen
# that's necessary as otherwise removing the language packs takes ages
# TODO: get rid of dpkg-reconfigure here
locale-gen
#TODO remove; are the other locales left on the system? -> disk space?
#dpkg-reconfigure locales
echo -e "#livecd \n\n" 				> /etc/environment
echo -e "LANGUAGE=\"$LANG_LIVE:$1\" \n\n" 	>> /etc/environment
echo -e "LANG=$LANG_LIVE" 			>> /etc/environment



# ========================================================================= #
#									    #
# 	Packages to be removed						    #
#  									    #
# ========================================================================= #

# TODO: there are dupes here!

# -----------------------------------------------------------------------
# language related packages
# -----------------------------------------------------------------------
# 	TODO: dpkg -l to check if there are more than these
#
apt-get --assume-yes remove --purge \
language-pack-ar-base language-pack-bn-base language-pack-ca-base 	\
language-pack-de-base language-pack-en-base language-pack-es-base 	\
language-pack-fr-base language-pack-hi-base language-pack-ja-base 	\
language-pack-pt-base language-pack-ru-base language-pack-zh-base

apt-get --assume-yes remove --purge \
myspell-en-gb myspell-en-us openoffice.org-help-en 	\
openoffice.org-thesaurus-en-us aspell-en mozilla-firefox-locale-en-gb




# ----------------------------------------------------------------------
# "useless" big packages
# ----------------------------------------------------------------------
apt-get --assume-yes remove --purge $REMOVELIST


apt-get --assume-yes remove --purge $REMOVELIST_LOCALE



# =========================================================================
#
# 	Packages to be installed
#
# =========================================================================


# -------------------------------------------------------------------------
# language packages
# -------------------------------------------------------------------------
# TODO: this is obsolete
apt-get --assume-yes --force-yes install $LANG_PACKAGES


# -------------------------------------------------------------------------
# gnome stuff
# -------------------------------------------------------------------------
apt-get --assume-yes --force-yes install $ADDLIST

apt-get --assume-yes --force-yes install $ADDLIST_LOCALE

#--------------------------------------------------------------------------
# update, for security (if running stable) or freshness (for unstable)
#--------------------------------------------------------------------------

apt-get --assume-yes dist-upgrade

# =========================================================================
#
# 	Configure Gnome (splash, theme, background etc.) 
#
# =========================================================================

# gnome-splash
# ------------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /apps/gnome-session/options/splash_image /usr/share/pixmaps/splash/gnome-splash1.png


# gnome theme
# -----------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /desktop/gnome/interface/gtk_theme Clearlooks


# gnome bg image
# --------------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /desktop/gnome/background/picture_filename /usr/share/pixmaps/gnome-background.jpg


# gnome bg color
# --------------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /desktop/gnome/background/primary_color "#2A569D"


# gnome nautilus spatial normal
# -----------------------------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type binary 							\
	--set /apps/nautilus/preferences/no_ubuntu_spatial true

# gnome url handlers
# ------------------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /gnome/url-handlers/https/command epiphany
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /gnome/url-handlers/http/command epiphany
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /gnome/url-handlers/about/command epiphany
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /gnome/url-handlers/unknown/command epiphany

# update epiphany homepage
# ------------------------
gconftool-2 								\
	--direct 							\
	--config-source xml:readwrite:/etc/gconf/gconf.xml.defaults 	\
	--type string 							\
	--set /apps/epiphany/general/homepage http://google.com/



# clean up: killing gconfd-2 -> otherwise unmount not possible
# (the gconftool --direct switch seems sometimes not to work, dunno why)
sleep 2
killall gconfd-2
/etc/init.d/hula stop
/etc/init.d/jabberd2 stop