0fSPARAMETRES D'AMORCAGE SPECIAUX - SYSTEME DE FICHIER D'AMORCAGE07                                    09F707

Vous pouvez utilise les parametres d amorcage suivants a l invite de commandes
 0fboot:07 en combinaison avec la methode damorcage (voir <09F307>). 
Ces parametres controlent comment le systeme de fichier d amorcage fonctionne.
0f
RESULTAT                                PARAMETRE07
Deboguage verbeux                       0fDEBCONF_DEBUG=507
Debogue la sequence d amorcage          0fBOOT_DEBUG=107
Desactive le framebuffer                0fdebian-installer/framebuffer=false07
Ne teste pas l USB                      0fdebian-installer/probe/usb=false07
Ne lance pas le PCMCIA                  0fhw-detect/start_pcmcia=false07
Force la configuration du reseau statique
					0fnetcfg/disable_dhcp=true07
Definit le clavier                      0fbootkbd=es07
Desactive l ACPI pour les cartes PCI (pratique pour certains serveurs HP et
machine basé sur le chipset Via)        0fpci=noacpi07

Par exemple :

  boot: live debian-installer/framebuffer=false

Appuyer sur F1control et F ensuite sur 1 pour l index d aide ou sur ENTER pour 
