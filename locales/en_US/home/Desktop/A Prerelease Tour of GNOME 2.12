<!DOCTYPE html PUBLIC "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><head><title>A Prerelease Tour of GNOME 2.12</title>


 
  
  <style type="text/css">
   <!--
   	html {
		/* background-color: #e0e0e0; */
	}
   	h1 {
		font-family: sans-serif;
		font-size: 300%;
		color: #6e7e97;
		margin-top: 0;
		padding-top: 0;
		padding-left: 1ex;
	}
	h2 {
		font-family: sans-serif;
		font-size: 180%;
		color: #6e7e97;
		margin-left: 10%;
		margin-right: 10%;
	}
	h3 {
		font-family: sans-serif;
		font-size: 120%;
		color: #6e7e97;
		margin-left: 12%;
		margin-right: 10%;
	}
	p {
		margin-left: 15%;
		margin-right: 10%;
		color: #444444;
	}
	p.image {
   		font-style: italic;
		text-align: center;
		margin-left: 0;
		margin-right: 0;
		margin-bottom: 2.5em;
	}
	p.greyed {
		text-align: center;
		border-style: solid;
		border-width: 1px;
		border-color: #6e7e97;
		color: #444444;
		padding: 1ex;
		margin-left: 10%;
	}
	p.h3 {
		margin-left: 20%;
		margin-right: 10%;
	}
	table {
		width: 100%;
	}
	img {
		border-style: none;
	}
   -->
   </style></head><body>
  <h1>A Prerelease Tour of GNOME 2.12</h1>
  <p class="greyed">
   GNOME 2.12 will be released to the world on September 7<sup>th</sup>, 2005,
   culminating 6 months of very exciting work by members of the project. A
   number of exciting technologies come together in GNOME 2.12 that will set
   the standard for free software desktops to come. Here is a sample
   (by no means an exhaustive list) of some of
   the outstanding work that has gone into GNOME thanks to its many
   contributors.
  </p>

  <h2>New Default Theme</h2>
  <p>
   GNOME 2.12 is going to be clearer and better looking then ever before.
   Clearlooks, the new theme engine that has proven incredibly popular with
   users, developers and distributors, is going to become the default theme for
   GNOME 2.12.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/clearlooks.png" alt="Theme Manager"><br>
   introducing... Clearlooks
  </p>
  <p>
   All of the screenshots have been done in Clearlooks. A theme that we hope
   will be the default choice of every GNOME vendor, giving GNOME a unified
   face, no matter which vendor you choose.
  </p>

  <h2>Core Technology</h2>
  <h3 id="gtk">GTK+ 2.8</h3>
  <p>
   GNOME 2.12 will utilise the latest and greatest in graphics toolkits through
   GTK+ 2.8. Still in its final stages of testing, GTK+ 2.8 will offer
   developers features not currently available in any other toolkit.
   Integration with the Cairo vector graphics library allows for smoother
   edges, RGBA translucency and better looking, more flexible theming.
  </p>
  <p>
   From the developer's view, the new GTK+ remains 100% compatible with all
   releases in the GTK+ 2.x tree, so you can effortlessly begin using the new
   features of GTK+ 2.8 without first requiring a complicated port.
  </p>
  <p>
   In the future, the GTK+/Cairo integration will allow for graphics
   accelerated through OpenGL by the Glitz library as well as easier printing
   without having to implement separate printing code. This will allow for
   better printing in more GNOME applications.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/gtk-colour-wheel.png" alt="GTK+ 2.8"><br>
   edges are now smoother thanks to Cairo antialiasing
  </p>
  <p>
   Drag and drop handling has been improved and now previews blocks of text when
   you drag them.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/drag-n-drop-text.png" alt="GEdit"><br>
   preview text as you drag
  </p>

  <h3 id="clipboard-manager">Clipboard Management</h3>
  <p>
   New clipboard management, based on the Freedesktop.org specification
   and tightly integrated with GNOME, allows for objects to
   persist in the clipboard longer than the lifetime of an application. This
   means that if you cut or copy an object and then exit that application, the
   item you put on the clipboard will remain until you replace it. The new
   clipboard manager is technically superior to existing implementations and
   integrates tightly with specially designed GTK+ APIs, allowing for a faster
   and more flexible clipboard implementation.
  </p>

  <h3 id="hal">Hardware Abstraction Layer</h3>
  <p>
   More software is taking advantage of the Hardware Abstraction Layer from
   Project Utopia. HAL-aware applications can display more information to the
   user, as well as benefit from "it just works" plug and play style hardware
   support. GNOME-VFS in GNOME 2.12 has improved integration with HAL, and now
   gives more visual cues about the types and names of media devices.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/gnome-vfs.png" alt="Nautilus, Desktop and Panel"><br>
   displaying volume names and types courtesy of HAL
  </p>

  <h2>Core Applications</h2>
  <h3 id="nautilus">Nautilus</h3>
  <p>
   Nautilus is GNOME's file manager and wasn't neglected when the features
   and polish were added to GNOME 2.12. Along with over 200 bugs fixed, Nautilus
   now sports a spatial tree file view, popularised in the original Apple
   Macintosh operating systems.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/nautilus-list.png" alt="Nautilus"><br>
   Nautilus spatial tree view
  </p>
  <p>
   The browser mode in Nautilus has also gotten some attention, and now has a
   side bar that shows your bookmarks, identically to in a file chooser or the
   Places menu on the panel.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/nautilus-browse.png" alt="Nautilus"><br>
   browser mode
  </p>
  <p>
   There is also an exciting list of new functionality extensions ready for
   use in Nautilus. Including a new "Send to..." feature for transferring
   documents, photos, music, etc. via Email, Instant Messaging or Bluetooth.
  </p>
  <p>
   These extensions are not officially part of the GNOME Desktop, but some of
   them may end up as default functionality in the future.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/nautilus-send-to.png" alt="Nautilus"><br>
   email an entire folder, compressed as a single zip file
  </p>
  <p>
   Some old features in Nautilus, such as the Open Terminal, that would be
   available on the desktop context menu, have now been moved into more powerful
   extensions, with more functionality for the power users that need them.
  </p>

  <h3 id="epiphany">Epiphany</h3>
  <p>
   Based off the Gecko rendering engine used in Firefox and other popular web
   browsers, Epiphany is default web browser for the GNOME desktop environment.
   Epiphany both has new features and more extensions in GNOME 2.12, including
   the ability to share bookmarks over Bonjour, ne Rendezvous and extensions to
   provide support for popular web technologies like Greasemonkey.
  </p>
  <p>
   With the release of Gecko 1.8, Epiphany will continue to strive towards
   GNOME's design goals, offering more useful error messages and clearer
   searching.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/epiphany-errorpage.png" alt="Epiphany"><br>
   more helpful errors with Gecko 1.8
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/epiphany-search.png" alt="Epiphany"><br>
   better searching of web pages
  </p>
  <p>
   Epiphany extensions offer the ability to extend Epiphany in new ways.
   When combined with the popular RSS feed reader Liferea, Epiphany can handle
   RSS and other news feeds straight from the browser.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/epiphany-rss.png" alt="Epiphany"><br>
   stay abreast of the important news
  </p>

  <h3 id="evolution">Evolution</h3>
  <p>
   Evolution is the popular mail client for GNOME that was developed by
   Ximian/Novell. Evolution sports a cleaner interface in GNOME 2.12 and a
   number of extra productivity features.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/evolution-red-line.png" alt="Evolution"><br>
   the Marcus Bains line
  </p>
  <p>
   The new Evolution also has a raft of new plugins available for users,
   including the support for inline PGP handling, a long-time requested feature,
   and the ability to do inline media playing.
   Additionally, Evolution's mail talking library libcamel has been moved into
   Evolution Data Server and made available to developers.
  </p>
  <p>
   Work in Evolution is now focusing on performance. While already able to
   handle huge inboxes, the Evolution team wants to reduce memory usage while
   further increasing speed and capacity. The improvements are already
   noticeable. Work is also taking place on supporting CalDAV (calendaring over
   WebDAV) in Evolution, allowing for easy shared calendaring and integration
   with the Hula mail and calendar server.
  </p>

  <h3 id="control-centre">Control Center</h3>
  <p>
   A new configuration applet (or capplet) in 2.12 is the <i>About Me</i>
   dialog. This dialog does not send any personal information to the Internet.
   It simply stores a vCard in Evolution's data server that is accessible from
   applications on your desktop. You might like to beam it to a phone via
   Bluetooth, or simply attach the data to emails when asked for contact
   information.
  </p>
  <p>
   System stored user information (GECOS) will also be updated if you have
   permission, allowing you to easily change your default details in legacy UNIX
   applications. In the future, more applications will be able to make use of
   this information through the Evolution Data Server.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/about-me.png" alt="About Me"><br>
   give GNOME some information about yourself
  </p>
  <p>
   Support for the custom mouse cursors extension has been merged into the
   GNOME control centre and no longer requires an additional application to
   set your preferred mouse theme. These cursors themes will instantly apply
   with GTK+ 2.8.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/mouse-capplet.png" alt="Mouse Properties"><br>
   choose a cursor theme
  </p>

  <h3 id="system-tools">System Tools</h3>
  <p>
   The default GNOME System Tools, a suite of standardised, distribution
   agnostic
   tools for managing your desktop machine, now include a tool to configure your
   startup services as well as giving users the ability to start and stop
   services on demand.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/services-admin.png" alt="Services Admin"><br>
   configure your services
  </p>

  <h3 id="panel-applets">Panel &amp; Applets</h3>
  <p>
   There have been lots of minor improvements in the GNOME panel and its
   applets.
  </p>
  <p>
   In keeping with the theme of improved multimedia handling, the Disk
   Mounter applet now offers intelligent actions based on the type of media
   it has detected. This means that if a user has a DVD or CD in their computer,
   they can have the option to play it again, right from the panel.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/applet-drivemount.png" alt="Drivemount"><br>
   choose to play the CD
  </p>
  <p>
   The battery monitor has an experimental backend that utilises the Hardware
   Abstraction Layer. This backend will eventually become the default, and allow
   us to support more types of batteries across more platforms without requiring
   upgrades to GNOME.
  </p>
  <p>
   The weather reporter now allows you to quickly search for your location,
   useful for when you don't know what state you're in.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/gweather.png" alt="Gweather"><br>
   find your location
  </p>
  <p>
   The panel will also include an integrated menu editor compliant with the
   Freedesktop.org menu specification. This menu editor is not yet fully
   featured, however offers most of the basic functionality for systems
   administrators wanting to lock down menu items. Using the Freedesktop.org
   menu specification allows the user to choose any menu editor he/she wants,
   and there are several excellent third-party menu editors currently being
   developed also.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/panel-editor.png" alt="Menu Editor"><br>
  </p>
  <p>
   The panel also has improved support for less common layout options. Vertical
   panels are now handled better than before, with more panel objects now
   supporting vertical panels.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/panel-sideways.png" alt="Panel"><br>
   vertical menus
  </p>

  <h2>Multimedia</h2>
  <h3 id="totem">Totem</h3>
  <p>
   Totem is the multimedia player that is shipped with the GNOME Desktop. Based
   off the powerful GStreamer multimedia framework, Totem can play almost any
   media format available today. Additionally, the plugin nature of the
   GStreamer framework enables vendors to ship licensed, non-free plugins for
   media formats which are not supported by a free codec or implementation.
  </p>
  <p>
   Totem and GStreamer embrace freedom and resultantly, have world class
   implementations of the Ogg Vorbis and Ogg Theora formats for audio and video
   respectively. Totem is the client of choice for connecting to the Flumotion
   streaming server, and it has been used to stream GUADEC and linux.conf.au.
  </p>
  <p>
   A highly visible improvement in Totem 2.12 is the new playlist sidebar. For
   developers, this playlist handling is handled by a library <i>plparser</i>,
   available for your use in other applications.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/totem-sidebar.png" alt="Totem"><br>
   Totem with its new sidebar
  </p>
  <p>
   The GStreamer backend now has full support for DVD menus and other DVD
   features, making it fully featured, if not more featured then the alternate
   Xine backend to Totem.
  </p>
  <p>
   Work has also begun on a Totem Mozilla plugin, allowing Totem's powerful
   multimedia handling inside Mozilla based web browsers, such as Firefox and
   Epiphany.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/totem-mozilla.png" alt="Mozilla"><br>
   a movie trailer playing inside Firefox
  </p>

  <h3 id="sound-juicer">Sound Juicer</h3>
  <p>
   While the most common and flexible format for purchasing music is still the
   compact disc, most people prefer their music in a more portable form. Sound
   Juicer imports CDs into your computer's music library. Again, built off the
   GStreamer framework, SJ can import your music using any format you have a
   plugin for. Ogg Vorbis, MP3, AAC, FLAC... anything. Configurable
   profiles also give you the ability to tweak your encoding for the best
   listening experience.
  </p>
  <p>
   By popular demand, Sound Juicer 2.12 also has a feature to preview songs
   before you import them. Using the same digital audio extraction (DAE)
   techniques used in importing your music, SJ can play CDs on machines with no
   analog audio, such as some Dell and Apple laptops.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/sound-juicer.png" alt="Sound Juicer"><br>
   playing a CD
  </p>

  <h3 id="nautilus-multimedia">Nautilus</h3>
  <p>
   Nautilus is now more tightly integrated with the multimedia tasks you want to
   do. In addition to the thumbnailing of video and previewing of audio in past
   releases, Nautilus now offers drag and drop capabilities from audio CDs and
   the ability to play a track simply by double clicking.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/nautilus-multimedia.png" alt="Nautilus"><br>
   audio CDs in nautilus
  </p>
  <p>
   Finally, due to popular demand, audio CD copying has been added to Nautilus'
   CD burning functionality.
  </p>
  <p class="image">
   <!-- FIXME - image -->
  </p>

  <h2>Extra Applications</h2>
  <h3 id="search-tool">Search Tool</h3>
  <p>
   The GNOME Search Tool now shows file previews, making it even easier to find
   your files.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/search-tool.png" alt="Search Tool">
  </p>

  <h2>New Applications</h2>
  <h3 id="evince">Evince</h3>
  <p>
   Evince is a new document reader to replace GGV and GPDF. Using the new
   libpoppler from freedesktop.org to render PDFs and utilising Cairo vector
   rendering throughout, Evince offers fulls text searching, text copying,
   thumbnailing, document bookmarks, a variety of scrolling modes and much more.
   As well as PDF and PS documents, Evince can (or will soon) handle DVI, and
   TIFF files, as well as OpenOffice.org Impress and Microsoft Powerpoint
   presentations.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/evince.png" alt="Evince"><br>
   viewing a PDF document
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/evince-dual.png" alt="Evince"><br>
   multiple views, full text searching, thumbnails and clipboard capabilities
  </p>
  <p>
   Evince also extends Nautilus, allowing some more document types to be
   previewed.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/nautilus-pdf-previews.png" alt="Nautilus"><br>
   preview PDFs in Nautilus
  </p>

  <h3 id="keyring-manager">Keyring Manager</h3>
  <p>
   GNOME has used a keyring for an ever growing number of authentication tasks
   since its addition just over a year ago. The keyring manager offers an
   interface to inspect your keyrings, make changes to keys or remove sensitive
   keys. It is one of the first applications completely developed by the GNOME
   Love project, and we're proud to finally have it on board.
  </p>
  <p class="image">
   <img src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/keyring-manager.png" alt="Keyring Manager"><br>
   key management
  </p>
  
  <!-- Footer -->
  <p class="greyed">
   All comments/complains/queries/flames to
   <a href="mailto:davyd@madeley.id.au">Davyd Madeley</a>.<br>
   Thanks to Luis Villa and Ryan Lortie for their help.<br>
   GNOME © 1997-2005, Free Software Foundation and others<br>
   This page © 2005, Davyd Madeley<br>
   <br>
<!-- Creative Commons License -->
<a rel="license" href="http://creativecommons.org/licenses/by-sa/2.5/"><img alt="Creative Commons License" src="A%20Prerelease%20Tour%20of%20GNOME%202%20Files/somerights20.gif" border="0"></a><br>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/2.5/">Creative Commons Attribution-ShareAlike 2.5 License</a>.
<!-- /Creative Commons License -->

<!--
<rdf:RDF xmlns="http://web.resource.org/cc/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Work rdf:about="">
   <dc:date>2005</dc:date>
   <dc:creator><Agent>
      <dc:title>Davyd Madeley</dc:title>
   </Agent></dc:creator>
   <dc:rights><Agent>
      <dc:title>Davyd Madeley</dc:title>
   </Agent></dc:rights>
   <dc:type rdf:resource="http://purl.org/dc/dcmitype/Text" />
   <dc:source rdf:resource="http://www.gnome.org/~davyd/gnome&amp;#45;2&amp;#45;12/"/>
   <license rdf:resource="http://creativecommons.org/licenses/by-sa/2.5/" />
</Work>

<License rdf:about="http://creativecommons.org/licenses/by-sa/2.5/">
   <permits rdf:resource="http://web.resource.org/cc/Reproduction" />
   <permits rdf:resource="http://web.resource.org/cc/Distribution" />
   <requires rdf:resource="http://web.resource.org/cc/Notice" />
   <requires rdf:resource="http://web.resource.org/cc/Attribution" />
   <permits rdf:resource="http://web.resource.org/cc/DerivativeWorks" />
   <requires rdf:resource="http://web.resource.org/cc/ShareAlike" />
</License>

</rdf:RDF>
-->
  </p>
 
</body></html>